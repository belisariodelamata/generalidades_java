/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase3;

import clase2.Persona;

/**
 *
 * @author BELSOFT
 */
public class EjemploDeVariables {
    public static void main(String[] args) {
        //Tipos de Variables de Instancia
        Persona personaUno=new Persona();
        personaUno.setNombre("Belisario");
        personaUno.setApellido("De La Mata");
        
        Persona personaDos=new Persona();
        personaDos.setNombre("Adelmo");
        personaDos.setApellido("Martinez");

        System.out.println(personaUno.getNombre());
        System.out.println(personaDos.getNombre());
        
        //Tipos de Variables de Clase (Static)
        Persona.setPais("COLOMBIA");
        
        
    }
}
