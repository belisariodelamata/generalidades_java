package clase3;

/**
 *
 * @author BELSOFT
 */
public class Estudiante {
    
    //Tipos de Identificadores de Acceso:
    //Aplican para variables y para Metodos
    
    //Acceso por Defecto: Se puede acceder a las variables desde cualquier clase,
    //desde el mismo paquete de la Clase
    String nombre;
    String apellido;
    
    //Acceso privado: Solo puede acceder a la variable la misma clase
    private String segundoApellido;
    
    //Acceso publico: Puede acceder a la variable cualquier clase, desde
    //cualquier paquete
    public String nombreColegio;
    
    //Acceso protegido: Puede acceder a la variable cualquier hijo de la misma o
    //el mismo Paquete
    protected String nombreUniversidad;
    
    ///////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////    
    /////Encapsulamiento: Conversión del identificador de acceso para una variable
    private String pais;

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }
    
    
}
