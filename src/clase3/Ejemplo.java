/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase3;

/**
 * @author BELSOFT
 * @description Esta clase sirve para ejemplificar lo que sucede al momento
 * de insertar comentarios en Java
 */
public class Ejemplo {
    public static void main(String[] args) {
        //Hola Este es un comentario
        
        /*
            Hola, este es un comentario
            multilinea, no hay problema
            en lo que se escriba en este bloque
        */
        
    }
    
    /**
     * Este metodo es de Prueba para verificar la documentacion del metodo
     */
    private static void metodoDePrueba(){
        
    }
}
