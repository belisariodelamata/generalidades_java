package clase2;

/**
 * 
 * @author BELSOFT
 */
public class Persona {

    private static String pais="";
    
    private String nombre;
    private String apellido;

    ///Los Get y los Set son para la Obtención del Valor y para la Asignacion del Valor
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public static String getPais() {
        return pais;
    }

    public static void setPais(String aPais) {
        pais = aPais;
    }    
}
